package jcoaplogger;

import org.ws4d.coap.connection.BasicCoapChannelManager;
import org.ws4d.coap.interfaces.CoapChannelManager;
import org.ws4d.coap.interfaces.CoapMessage;
import org.ws4d.coap.interfaces.CoapRequest;
import org.ws4d.coap.interfaces.CoapServer;
import org.ws4d.coap.interfaces.CoapServerChannel;
import org.ws4d.coap.messages.CoapMediaType;
import org.ws4d.coap.messages.CoapResponseCode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.FileWriter;
import java.io.BufferedWriter;

public class Logger {
	private final int server_port = 61616;
	private final String logFile = "coap.log";


	private class CoAPServer implements CoapServer {
		private FileWriter log;
		private BufferedWriter logOut;

		public CoAPServer() {
			super();
			try {
				log = new FileWriter(logFile, true);
				logOut = new BufferedWriter(log);
				logOut.write("Log opened\n");
				logOut.flush();
			} catch (Exception e) {
				System.out.println("Opening file failed");
			}
		}

		@Override
		public CoapServer onAccept(CoapRequest request) {
			System.out.println("Accept connection");
			return this;
		}

		@Override
		public void onRequest(CoapServerChannel channel, CoapRequest request) {
			System.out.println(getDateTime() + " Received message: " + request.toString()+ " URI: " + request.getUriPath() + " Payload: " + new String(request.getPayload()));

			try {
				logOut.write(getDateTime() + " " + request.toString()+ " " + request.getUriPath() + " " + new String(request.getPayload()) + "\n");
				logOut.flush();
			} catch (Exception e) {
				System.out.println("Writing to log failed");
			}
		}

		@Override
		public void onSeparateResponseFailed(CoapServerChannel channel) {
			System.out.println("Separate response transmission failed.");
		}

		private String getDateTime(){
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			return dateFormat.format(date);
		}
	}

	private CoAPServer server;
	private CoapChannelManager channelManager;

	public Logger() {
	}


	public void run() {
		server = new CoAPServer();
		channelManager = BasicCoapChannelManager.getInstance();
		channelManager.createServerListener(server, this.server_port);
	}


}
