
package jcoaplogger;

import jcoaplogger.Logger;

public class Main {
	/**
	 * Initates the application.
	 *
	 */
	public static void main(String[] args) {
		Logger jcl = new Logger();
		jcl.run();
	}
}
